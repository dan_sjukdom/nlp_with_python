from word2vec import (
    makeVocab,
    Word2Vec
)


corpus = """
    El perro come y el gato tambien
    El gato esta jugando con el perro
    El gato y el perro comen en el suelo
    Un gato juega con un perro
"""


vocab = makeVocab(corpus)


V = list(vocab.items())[0][1]["index"] + 1
d = 100

w2v = Word2Vec(V, d)

print(w2v.U.shape)
print(w2v.W.shape)

x = w2v.forward("El", vocab)

print(x)

"""
    This algorithm was created by Mykolov to train a neural model using million of words
    and cluster them together in a vectorial space.

    The approach used in the paper is called skipgram and can use negative sampling.

    The meaning and semantic of words are encoded in the vectorial space.

    
"""


import numpy as np
from collections import OrderedDict


def init_weights(v, d, method):
    """
        Returns an initial weight matrix (embedding)
        shape: (v, d)
    """
    if method == "normal":
        weights = np.random.randn(v, d)
        return weights


def bigrams(sentence: str) -> list:
    """
        Returns the bigrams of the sentence adding the 
        <BOS> and <EOS> tokens.
    """
    sentence = f"<BOS> {sentence} <EOS>"
    tokens = sentence.split()
    return [ (wi, wj) for wi, wj in zip(tokens[:-1], tokens[1:]) ]


def softmax(Wi, Wo):
    """
        Modified softmax function
        Softmax function applied to word vectors
    """
    vocabSize = Wi.shape[0]
    numerator = np.exp(np.dot(Wi, Wo))
    denominator = np.sum([ np.dot(VectorMap[Wj], Wo) for Wj in range(vocabSize) ])
    return numerator/denominator



def makeVocab(corpus) -> OrderedDict:
    vocab = {}
    n = 0
    for sentence in corpus.split("\n"):
        for token in sentence.split():
            if token not in vocab:
                vocab[token] = {
                    "freq": 1,
                    "index": n
                }
                n += 1
            else:
                vocab[token]["freq"] += 1
    return OrderedDict(sorted(vocab.items(), key=lambda x: x[1]["index"], reverse=True))


class Word2Vec:
    """
        Word 2 vec neural network model (skipgram) based on the paper
        Distributed Representations of Words and Prases and their
        Compositionality.
    """
    def __init__(self, v, d):
        """
            Params:
            - d: embedding dimmension 
            - v: input size (vocab)
        """
        self.d = d
        self.v = v
        self.U = init_weights(v, d, method="normal")
        self.W = np.random.randn(d, v)


    def train(self, x, y, epochs):
        """
            Train the neural model using:
            - x: training data (word bigrams)
        """

        pass


    def forward(self, x: str, vocab: dict):
        """
            The vector-matrix multiplication it's reduced to a 
            look-up table which outputs the vector representation
            of the one hot encoded word in the vocabulary.
        """
        idx = vocab[x]["index"]
        h = self.U[idx, :]
        a = self.W[idx, :]
        y = a
        return y

    @staticmethod
    def backwards(x, lr):
        pass
